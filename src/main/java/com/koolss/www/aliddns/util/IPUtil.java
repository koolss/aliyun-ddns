package com.koolss.www.aliddns.util;

import cn.hutool.core.lang.Validator;
import cn.hutool.http.HttpUtil;
import cn.hutool.log.StaticLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPUtil {
    /**
     * 获取公网IP地址
     *
     * @return String
     */
    public static String getIP() {
        String result = HttpUtil.get("whatismyip.akamai.com");
        if (!Validator.isIpv4(result)){
            StaticLog.error("获取IP地址失败");
        }
        return result;
    }

    public static void main(String args[]) {
        System.out.println(IPUtil.getIP());
    }


    public static String execCurl(String[] cmds) {
        ProcessBuilder process = new ProcessBuilder(cmds);
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            return builder.toString();
        } catch (IOException e) {
            StaticLog.error("获取IP地址失败");
            e.printStackTrace();
        }
        return null;
    }
}
