package com.koolss.www.aliddns;

import com.koolss.www.aliddns.config.Config;
import com.koolss.www.aliddns.service.IDDnsService;
import com.koolss.www.aliddns.service.impl.DDnsServiceImpl;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AppRun {

    private static final IDDnsService ddnsService = new DDnsServiceImpl();

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ddnsService.checkRecord();
            }
        };
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(runnable,0, Config.getConfig().interval(), TimeUnit.SECONDS);
    }
}
