package com.koolss.www.aliddns.service;

import com.aliyuncs.alidns.model.v20150109.DescribeSubDomainRecordsResponse;

import java.util.List;

/**
 * @ClassName IDDnsService
 * @Description: DDns接口
 * @Author admin
 * @Date 2020/9/30 16:28
 * @Version 1.0.0
 * @Email koolss@koolss.com
 **/
public interface IDDnsService {

    /**
     * 添加解析记录
     *
     * @param subdomainName 子域名
     * @param value         记录值
     * @return
     */
    boolean addDomainRecord(String subdomainName, String value);

    /**
     * 删除解析记录
     *
     * @param recordId 记录值
     * @return
     */
    boolean deleteDomainRecord(String recordId);

    /**
     * 修改解析记录
     *
     * @param recordId      记录ID
     * @param subdomainName 子域名
     * @param value         记录值
     * @return
     */
    boolean updateDomainRecord(String recordId, String subdomainName, String value);

    /**
     * 获取子域名解析记录列表
     * @param subDomain 完整子域名
     * @return
     */
    List<DescribeSubDomainRecordsResponse.Record> describeSubDomainRecords(String subDomain);

    /**
     * 检查域名
     */
    void checkRecord();
}
