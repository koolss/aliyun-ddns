package com.koolss.www.aliddns.service.impl;

import cn.hutool.log.StaticLog;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.alidns.model.v20150109.*;
import com.aliyuncs.exceptions.ClientException;
import com.koolss.www.aliddns.config.Config;
import com.koolss.www.aliddns.config.IConfig;
import com.koolss.www.aliddns.service.IDDnsService;
import com.koolss.www.aliddns.util.IPUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName DDnsServiceImpl
 * @Description: DDNS实现类
 * @Author admin
 * @Date 2020/9/30 16:29
 * @Version 1.0.0
 * @Email koolss@koolss.com
 **/
public class DDnsServiceImpl implements IDDnsService {
    //配置文件
    private static final IConfig config = Config.getConfig();

    @Override
    public boolean addDomainRecord(String subdomainName, String value) {
        IAcsClient client = config.getClient();
        AddDomainRecordRequest request = new AddDomainRecordRequest();
        request.setDomainName(config.domainName());
        request.setType("A");
        request.setValue(value);
        request.setRR(subdomainName);
        try {
            AddDomainRecordResponse acsResponse = client.getAcsResponse(request);
            return true;
        } catch (ClientException e) {
            e.printStackTrace();
            StaticLog.error("ErrCode:" + e.getErrCode());
            StaticLog.error("ErrMsg:" + e.getErrMsg());
            StaticLog.error("RequestId:" + e.getRequestId());
        }
        return false;
    }

    @Override
    public boolean deleteDomainRecord(String recordId) {
        IAcsClient client = config.getClient();
        DeleteDomainRecordRequest request = new DeleteDomainRecordRequest();
        request.setRecordId(recordId);
        try {
            DeleteDomainRecordResponse acsResponse = client.getAcsResponse(request);
            return true;
        } catch (ClientException e) {
            e.printStackTrace();
            StaticLog.error("ErrCode:" + e.getErrCode());
            StaticLog.error("ErrMsg:" + e.getErrMsg());
            StaticLog.error("RequestId:" + e.getRequestId());
        }
        return false;
    }

    @Override
    public boolean updateDomainRecord(String recordId, String subdomainName, String value) {
        IAcsClient client = config.getClient();
        UpdateDomainRecordRequest request = new UpdateDomainRecordRequest();
        request.setRecordId(recordId);
        request.setRR(subdomainName);
        request.setValue(value);
        request.setType("A");
        try {
            UpdateDomainRecordResponse acsResponse = client.getAcsResponse(request);
            return true;
        } catch (ClientException e) {
            e.printStackTrace();
            StaticLog.error("ErrCode:" + e.getErrCode());
            StaticLog.error("ErrMsg:" + e.getErrMsg());
            StaticLog.error("RequestId:" + e.getRequestId());
        }
        return false;
    }

    @Override
    public List<DescribeSubDomainRecordsResponse.Record> describeSubDomainRecords(String subDomain) {
        List<DescribeSubDomainRecordsResponse.Record> records = new ArrayList<DescribeSubDomainRecordsResponse.Record>();
        IAcsClient client = config.getClient();
        DescribeSubDomainRecordsRequest request = new DescribeSubDomainRecordsRequest();
        request.setSubDomain(subDomain);
        try {
            DescribeSubDomainRecordsResponse acsResponse = client.getAcsResponse(request);
            records.addAll(acsResponse.getDomainRecords());
        } catch (ClientException e) {
            e.printStackTrace();
            StaticLog.error("ErrCode:" + e.getErrCode());
            StaticLog.error("ErrMsg:" + e.getErrMsg());
            StaticLog.error("RequestId:" + e.getRequestId());
        }
        return records;
    }

    @Override
    public void checkRecord() {
        StaticLog.info("正在爬取主机公网IP地址...");
        String ip = IPUtil.getIP();
        StaticLog.info("检测域名解析...");
        String subdomainName = config.subdomainName();
        String fullDomainName = config.subdomainName() + "." + config.domainName();
        List<DescribeSubDomainRecordsResponse.Record> records = describeSubDomainRecords(fullDomainName);
        if (records.size() == 0){
            //添加解析记录
            this.addDomainRecord(config.subdomainName(),ip);
            StaticLog.info("添加解析记录：{}:{}",fullDomainName,ip);
        }else {
            //修改解析记录
            DescribeSubDomainRecordsResponse.Record record = records.stream().findFirst().get();
            if (!record.getValue().equals(ip)){
                this.updateDomainRecord(record.getRecordId(),subdomainName,ip);
                StaticLog.info("修改解析记录：{}:{}",fullDomainName,ip);
            }
        }
        StaticLog.info("检测完毕...");
    }
}
