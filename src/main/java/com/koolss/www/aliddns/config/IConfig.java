package com.koolss.www.aliddns.config;

import com.aliyuncs.IAcsClient;

/**
 * @ClassName IConfig
 * @Description: 配置文件
 * @Author admin
 * @Date 2020/9/30 16:05
 * @Version 1.0.0
 * @Email koolss@koolss.com
 **/
public interface IConfig {
    /**
     * AccessKeyId
     * @return
     */
    String getAccessKeyId();

    /**
     * AccessKeySecret
     * @return
     */
    String getAccessKeySecret();

    /**
     * 主域名
     * @return
     */
    String domainName();

    /**
     * 子域名
     * @return
     */
    String subdomainName();

    /**
     * 间隔时间
     * @return
     */
    Integer interval();

    /**
     * 获取客户端
     * @return
     */
    IAcsClient getClient();
}
