package com.koolss.www.aliddns.config;

import cn.hutool.log.StaticLog;
import cn.hutool.setting.dialect.Props;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @ClassName DDnsConfig
 * @Description: 配置文件
 * @Author admin
 * @Date 2020/9/30 16:10
 * @Version 1.0.0
 * @Email koolss@koolss.com
 **/
public class DDnsConfig implements IConfig {
    //指定配置文件地址resource目录下:
    private static Props prop = new Props();

    private static IAcsClient client = null;

    static {
        try {
            //尝试从外部读取ddns.properties文件
            FileInputStream inputStream = new FileInputStream("ddns.properties");
            prop.load(inputStream);
        } catch (IOException e) {
            StaticLog.error("读取配置文件失败，请把ddns.properties放在和jar包相同目录下");
            prop = Props.getProp("ddns.properties");
        }
    }

    @Override
    public String getAccessKeyId() {
        return prop.getStr("AccessKeyID");
    }

    @Override
    public String getAccessKeySecret() {
        return prop.getStr("AccessKeySecret");
    }

    @Override
    public String domainName() {
        return prop.getStr("DomainName");
    }

    @Override
    public String subdomainName() {
        return prop.getStr("SubdomainName");
    }

    @Override
    public Integer interval() {
        return prop.getInt("Interval");
    }

    /**
     * 获取客户端
     * @return
     */
    @Override
    public IAcsClient getClient() {
        if (client == null) {
            synchronized (DDnsConfig.class) {
                if (null == client) {
                    IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou",this.getAccessKeyId(),this.getAccessKeySecret());
                    client = new DefaultAcsClient(profile);
                }
            }
        }
        return client;
    }
}
