package com.koolss.www.aliddns.config;

/**
 * @ClassName Config
 * @Description: 配置文件
 * @Author admin
 * @Date 2020/9/30 16:51
 * @Version 1.0.0
 * @Email koolss@koolss.com
 **/
public class Config {

    private static IConfig config;

    /**
     * 获取配置文件
     * @return
     */
    public static IConfig getConfig(){
        if (config == null){
            synchronized (Config.class){
                if (config == null){
                    config = new DDnsConfig();
                }
            }
        }
        return config;
    }
}
